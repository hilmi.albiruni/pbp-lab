from django.shortcuts import redirect, render
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None, request.FILES or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return redirect("/lab-4/")
            
    context['form'] = form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = { 'notes' : notes }
    return render(request, 'lab4_note_list.html', response)