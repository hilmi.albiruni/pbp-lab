1. Apakah perbedaan antara JSON dan XML?

JSON (JavaScript Object Notation) adalah sebuah format ringkas pertukaran data komputer. JSON digunakan untuk menyimpan informasi dengan terorganisir dan mudha diakses. Selain itu, JSON juga mudah dibaca oleh manusia. 

XML (eXtensible Markup Language) adalah bahasa markup yang difungsikan untuk menyimpn data. XML dapat menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan.

# Perbedaan diantara kedua tersebut adalah:
> JSON untuk pertukaran data ringan yang jauh lebih mudah untuk mengurai data yang sedang dikirim, sedangkan XML untuk menyimpan dan mengangkut data dari satu aplikasi ke aplikasi lain melalui internet.
> JSON lebih mudah dibaca daripada XML.
> XML lebih baik dalam hal markup dokumen dan informasi metadata.
> XML tidak mendukung array, sedangkan JSON mendukung array.
> eksistensi file JSON adalah .json , sedangkan XML adalah .xml


2. Apakah perbedaan antara HTML dan XML?

HTML (HyperText Markup Language) adalah bahasa komputer untuk memposisikan tata letak dan konvensi pemformatan ke dokumen teks.

XML (eXtensible Markup Language) adalah bahasa markup yang difungsikan untuk menyimpn data. XML dapat menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan

# Perbedaan diantara kedua tersebut adalah:
> HTML berfokus pada penyajian data, sedangkan XML berfokus pada transfer data.
> XML menyediakan dukungan namespaces, sedangkan HTML tidak.
> XML strict pada tag penutup, sedangkan HTML tidak strict.
> HTML tidak terlalu sensitif pada huruf besar/kecil, sedangkan XML sangat sensitif.
> HTML sudah memiliki kode tag yang telah ditentukan sebelumnya, sedangkan XML harus didefinisikan tagnya oleh programmer.


# Sumber:
> https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
> https://www.monitorteknologi.com/perbedaan-json-dan-xml/
