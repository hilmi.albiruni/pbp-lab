import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xFFE8F7FC),
        appBar: AppBar(
          backgroundColor: Color(0xFF060C23),
          title: Text("Covid Assistant"),
          elevation: 20,
          actions: [
            IconButton(icon: Icon(Icons.account_circle_outlined), onPressed: (){}, color: Color(0xFF17C2EC))
          ],
          leading: Icon(Icons.menu),
        ),
        body : Center (
          child: Image(
            width: 200,
            height: 200,
            image: AssetImage('assets/main.png')),
          // child: Container(
          //   child: Text('Apa itu Covid Assistant ?'),
          // )
      ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Color(0xFF060C23),
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home'),),
            BottomNavigationBarItem(icon: Icon(Icons.add_alert_outlined), title: Text('Add Features'),),
            BottomNavigationBarItem(icon: Icon(Icons.settings), title: Text('Setting'),),
          ],
          ),
    )
    );
  }
}
