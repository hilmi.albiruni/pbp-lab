import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "Poppins",
        canvasColor: Color.fromRGBO(232, 247, 252, 1),
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Image.asset("assets/logo.png"),
          backgroundColor: Color.fromRGBO(6, 12, 35, 1),
          actions: <Widget>[
            new Container(),
            new Center(
                child: Text(
              'Home',
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontSize: 20.0,
                color: Colors.white,
              ),
            )),
          ],
        ),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(children: [
              SizedBox(
                height: 20,
              ),
              // TextField(),
              TextFormField(
                decoration: new InputDecoration(
                  labelText: "Nama Lengkap",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Nama tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              TextFormField(
                obscureText: true,
                decoration: new InputDecoration(
                  labelText: "Password",
                  icon: Icon(Icons.password),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Password tidak boleh kosong';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                child: Text(
                  "Login",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    print("Login berhasill");
                  }
                },
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
